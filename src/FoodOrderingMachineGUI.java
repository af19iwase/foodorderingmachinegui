import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {
    private JLabel topLabel;
    private JLabel middleLabel;
    private JLabel totalLabel;
    private JLabel sumLabel;
    private JLabel unitLabel;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton gyozaButton;
    private JButton yakisobaButton;
    private JButton checkButton;
    private JTextArea receivedInfo;
    private JPanel root;

    int price=0,payment=0;

    public FoodOrderingMachineGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=order("Tempura",price);
            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("kadai_tempura1.jpg")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=order("Ramen",price);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("kadai_ramen1.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=order("Udon",price);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("kadai_udon1.jpg")
        ));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=order("Karaage",price);
            }
        });
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("kadai_karaage1.jpg")
        ));
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=order("Yakisoba",price);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("kadai_yakisoba1.jpg")
        ));
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=order("Gyoza",price);
            }
        });
        gyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("kadai_gyoza1.jpg")
        ));
        checkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. Total price is "+String.valueOf(""+payment)+" yen");
                    receivedInfo.setText("");
                    sumLabel.setText("");
                    price=0;
                    payment=0;
                }
            }
        });
    }

    public static void main(String[] args) {
        javax.swing.JFrame frame = new javax.swing.JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    int order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+ food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        switch (food){
            case "Tempura":
                price=320;
                break;
            case "Karaage":
                price=360;
                break;
            case "Gyoza":
                price=390;
                break;
            case "Udon":
                price=470;
                break;
            case "Yakisoba":
                price=500;
                break;
            case "Ramen":
                price=520;
                break;
        }
        if (confirmation == 0) {
            receivedInfo.append(food+" "+price+" yen\n");
            payment+=price;
            sumLabel.setText(String.valueOf(" "+payment));
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+ food +"! It will be served as soon as possible.");
        }
        return price;
    }
}
